<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('hello_world','HelloController@hello_world');

Route::get('sites/export','SiteController@export');

Route::post('clone_site','SiteController@clone_site');

Route::post('backup_site','SiteController@backup_site');

Route::post('restore_backup_site','SiteController@restore_backup_site');

Route::get('sites/get_sites','SiteController@get_sites');

Route::get('sites/trash','SiteController@trash');

Route::get('sites/backups','SiteController@backups');

Route::post('delete_backup','SiteController@delete_backup');

Route::get('sites/suspend','SiteController@suspend');

Route::get('sites/site_continue','SiteController@site_continue');

Route::get('sites/restore','SiteController@restore');

Route::post('force_delete', 'SiteController@force_delete');

Route::get('update_license','OregisterController@update_license');

Route::get("reload_server",'SiteController@reload_server');

Route::get('register_your_license','OregisterController@register_your_license');

Route::resource("sites","SiteController");
Route::resource("options","OptionController");

// Authentication routes...
/*
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
*/

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');

/**
 * Register Route(s)
 */
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

//Admin Routes
Route::get('/list_users', 'AdminController@list_users');
Route::get('/edit_user', 'AdminController@edit_user');
Route::get('/create_user', 'AdminController@create_user');
Route::post('/delete_user', 'AdminController@delete_user');
Route::get('/phpmyadmin_panel','AdminController@phpmyadmin_panel');
Route::get('/phpinfo_panel','AdminController@phpinfo_panel');
Route::post('/change_php','AdminController@change_php');
Route::post('/store_user','AdminController@store_user');
Route::post('/update_user','AdminController@update_user');

//BASE ROUTES
Route::get('/premium_plugins','BaseController@premium_plugins');
Route::post('/pete_plugins_install','BaseController@pete_plugins_install');
Route::post('/pete_plugins_update','BaseController@pete_plugins_update');
Route::post('/pete_update','BaseController@pete_update');
Route::post('/validate_pete','BaseController@validate_pete');
Route::get('/get_ads','BaseController@get_ads');
Route::get('/phpinfo_view','BaseController@phpinfo_view');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::get('/get_domains', function () {
	
	$sites = Site::orderBy('id', 'desc')->get();
    return response()->json($sites);
});


Route::get('/', array('as' => 'sites.index', 'uses' => 'SiteController@index'));


