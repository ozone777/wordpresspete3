<?php

namespace App;
use Log;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    //
	protected $table = 'options';
	
	public static function delete_meta_value($key){
		$object = Option::where('option_name',$key)->first();
		if($object){
		   $object->delete();
		}
	}
	
	public static function get_meta_value($key){
		$object = Option::where('option_name',$key)->first();
		if(isset($object)){
			return $object->option_value;
		}else{
			return "";
		}		
	}
}
