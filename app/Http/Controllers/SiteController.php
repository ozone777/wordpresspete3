<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use App\Site;
use App\Adomain;
use App\Secure;
use App\User;
use App\Backup;
use Illuminate\Support\Facades\Input;
use Response;
use App\App;
use Validator;
use App\Option;
use DB;
use App\Pete;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Redirect;
use App\PeteOption;
use Illuminate\Routing\Route;
use Illuminate\Support\MessageBag;
use View;

class SiteController extends Controller {

	
	public function __construct(Request $request){
			
	    $this->middleware('auth');
		$dashboard_url = env("DASHBOARD_URL");
		$viewsw = "/sites";
		
		//DEBUGING PARAMS
		$debug = env('DEBUG');
		if($debug == "active"){
			$inputs = $request->all();
			Log::info($inputs);
		}
		
		$system_vars = parent::__construct();
		$pete_options = $system_vars["pete_options"];
		$sidebar_options = $system_vars["sidebar_options"];
		$os_distribution = $system_vars["os_distribution"];
		
		View::share(compact('dashboard_url','viewsw','pete_options','system_vars','sidebar_options','os_distribution'));	
	 }
	 
	 
	
	public function register_your_license(){
		
	
		$validation_url = Option::get_meta_value('validation_url');
		return view('oregisters.register_your_license', compact('validation_url'));
		
	}
	
	
	public function export(){
		
		$user = Auth::user();
		$tab_index = "index";
		$site = Site::findOrFail(Input::get('id'));
		
		if($site->app_name=="WordPress"){
			$site->export_wordpress();
		}else if(($site->app_name=="Drupal") || ($site->app_name=="drupal")){
			$site->export_drupal();
		}
		
		$exporturl = $site->file_to_download;
		
		if($user->admin){
			$sites = Site::orderBy('id', 'desc')->paginate(50);
		}else{
			$sites = $user->my_sites()->paginate(10);
		}
		
		
		return view('sites.index', compact('sites','exporturl','tab_index'));
	  
	}
	
	public function reload_server(){
		
		Site::reload_server();
		return response()->json(["OK" => "OK"]);  
	}
	
	public function trash(){
		
		$user = Auth::user();
		$sites = $user->my_trash_sites()->where("app_name","WordPress")->paginate(10);
		$tab_index = "trash";
		return view('sites.trash', compact('sites','tab_index'));
		
	}
	
	public function suspend(){
		
		$site = Site::findOrFail(Input::get('id'));
		$site->suspend = true;
		$site->suspend_wordpress();
		$site->save();
		return Redirect::to('/?success=true&site_id='.$site->id);
	  
	}
	
	public function site_continue(){
		
		$site = Site::findOrFail(Input::get('id'));
		$site->suspend = false;
		$site->continue_wordpress();
		$site->save();
		return Redirect::to('/?success=true&site_id='.$site->id);
	  
	}
	
	public function get_sites(){
		
		$app_name = Input::get('app_name');
		if($app_name == "Wordpress"){
		  $result = Site::where('app_name', "Wordpress")->orderBy('id', 'desc')->get();
		}else if($app_name == "Wordpress+laravel"){
		  $result = Site::where('app_name', "Wordpress")->orderBy('id', 'desc')->get();
		  for ($i = 0; $i < count($result); $i++) {
			if(substr_count($result[0]->domain, '.') > 2) {
				unset($array[$i]);
			}
			  
		  }
		}
	    return response()->json($result);
	  
	}
	
	
	public function restore(){
		$site = Site::withTrashed()->findOrFail(Input::get('id'));
		$site->restore();
		$site->restore_wordpress();
		return Redirect::to('/?success=true&site_id='.$site->id);
	}
	
	public function clone_site(){
		
		$user = Auth::user();
		$clone_domain = Input::get('clone_domain');
		$site_to_clone_id = Input::get('site_to_clone_id');
		$clone_domain = preg_replace("/\s+/", "", $clone_domain);
		$site_name = str_replace(".","",$clone_domain);
		
		if($clone_domain == ""){
			return response()->json(['message'=> "Empty Domain"]);
		}
		
		$site = Site::where("url",$clone_domain)->first();
		if(isset($site)){
			return response()->json(['message'=> "Domain Taken"]);
		}
		
		if($site_name){
			
			$forbidden_names = array("filemanager", "ssl", "localhost", "pete", "pete2","html", "Pete", "Pete2");
		
			if (in_array($site_name, $forbidden_names)) {
				return response()->json(['message'=> "Forbidden project name"]);
			}
		}
			
		$site_to_clone = Site::findOrFail($site_to_clone_id);
		$backup = $site_to_clone->snapshot_creation("oclone");	

		$base_path = base_path();
		$backup_file = "$base_path/backups/$backup->site_id/".$backup->file_name;	
			
		$site = new Site();
		$site->action_name = "Clone";
		$site->name = $site_name;
		$site->url = $clone_domain;
		$site->wp_user = $backup->wp_user;
		$site->theme = $backup->theme;
		$site->user_id = $user->id;
		$site->first_password = $backup->first_password;
		$site->barserver_id = $backup->barserver_id;
		$site->backup_days = 2;
		$db_name = "db_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		$db_user = "usr_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		$db_user_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10); 
		$site->import_wordpress($backup_file,["db_name" => $db_name, "db_user" => $db_user, "db_user_pass" => $db_user_pass]);

		$backup->delete();
		
		return response()->json(['ok' => 'OK']);
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Auth::user();	
		if($user->admin){
			$sites = Site::orderBy('id', 'desc')->where("app_name","WordPress")->paginate(50);
		}else{
			$sites = $user->my_sites()->where("app_name","WordPress")->paginate(10);
		}
		
		$success = Input::get('success');
		$site_id = Input::get('site_id');
		$tab_index = "index";
		$current_user = Auth::user(); 
		return view('sites.index', compact('sites','success','site_id','tab_index','current_user'));
	}
	
	public function search()
	{
		$viewsw = "sites";
	    $sites = Site::orderBy('id', 'desc')->where('url', 'like', '%' . Input::get('data') . '%')->paginate(50);
		$data = Input::get('data');
		return view('sites.index', compact('sites','current_user','first_time','data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{   
		$current_user = Auth::user(); 
		return view('sites.create',compact('current_user'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		$pete_options = new PeteOption();
		$user = Auth::user();
		$fields_to_validator = $request->all();
		
		$site = new Site();
		$site->output = "";
		$site->user_id = $user->id;
		
		$site->action_name = "New";
		$site->to_clone_project_id = $request->input("to_clone_project_id");
		$site->name = $request->input("name");
		$site->to_import_project = $request->input("to_import_project");
		$site->user_id = $user->id;
		$site->url = $request->input("url");
		
		$site->url = preg_replace("/\s+/", "", $site->url);
		$site->name = preg_replace("/\s+/", "", $site->name);
		
		if($pete_options->get_meta_value('domain_template')){
	
			$site->url = $site->url . "." . $pete_options->get_meta_value('domain_template');
		}
	
		if($site->name){
			
			$forbidden_names = array("filemanager", "ssl", "localhost", "pete", "pete2","html", "Pete", "Pete2");
		
			if (in_array($site->name, $forbidden_names)) {
			    return redirect('sites/create')->withErrors("Forbidden project name");
			}
		}
		
    	$validator = Validator::make($fields_to_validator, [
	   	 'name' =>  array('required', 'regex:/^[a-zA-Z0-9-_]+$/','unique:sites'),
       	 'url' => 'required|unique:sites',
    	 ]);
		 
      	if ($validator->fails()) {
			
 	    	return redirect('sites/create')
 	    		->withErrors($validator)
 	    			->withInput();
 			
      	 }
		
		$site->new_wordpress();
		$site->save();
			
		return Redirect::to('/sites/'.$site->id .'/edit' .'?success=' . 'true');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$site = Site::findOrFail($id);
		$adomains = Adomain::orderBy('id', 'desc')->where('site_id', $site->id)->get();	
		$success = Input::get('success');
		return view('sites.edit', compact('site','adomains','success'));
	}
	
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = Auth::user();
		$site = Site::findOrFail($id);
		
		if(($user->id == $site->user_id) || ($user->admin == true)){
			$site->delete_wordpress();
			$site->delete();
			$debug = env('DEBUG');
			if($debug == "active"){
				Log::info('Ouput deleteDebug' . $site->output);
			}
			
		}
		
		return Redirect::to('/?success=true');
	}
	
    public function force_delete(){
	   
	    $user = Auth::user();
 		$site = Site::onlyTrashed()->findOrFail(Input::get('site_id'));	
 		$site->force_delete_wordpress();
 		
		if($user->admin){
			$site->forceDelete();
		}else if($user->id == $site->user_id){
			$site->forceDelete();
		}
		
 		return Redirect::to('sites/trash');
	
    }
	
}
