=== Plugin Name ===
Contributors: peterconsuegra, ozone, laurabusche
Tags: akismet, clone WordPress, laravel, laravel Integration, laravel wordpress, export wordpress, install local
Requires at least: 4.0
Tested up to: 4.9.6
License: GPLv2 or later

How this plugins work? Changing the cookie default path this will allow WordPress cookies to be available to test.example.com or something.example.com. Simply tick the tickbox Allow Cookies to go across All Subdomains with an option to go across subdomains.

== Description ==

Get the best of both worlds by integrating WordPress with Laravel – a powerful MVC framework for PHP. This plugin integrates WordPress with Laravel in an optimal way, while preserving the WordPress core intact. 

