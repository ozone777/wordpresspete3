<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use App\Option;
use Illuminate\Http\Request;
use View;
use Illuminate\Support\Facades\Auth;

class OptionController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function __construct(Request $request){
	    
	    $this->middleware('auth');
		$this->middleware('auth.admin');
		
		$dashboard_url = env("DASHBOARD_URL");
		$viewsw = "/options";
		
		//DEBUGING PARAMS
		$debug = env('DEBUG');
		if($debug == "active"){
			$inputs = $request->all();
			Log::info($inputs);
		}
		
		$system_vars = parent::__construct();
		$pete_options = $system_vars["pete_options"];
		$sidebar_options = $system_vars["sidebar_options"];
		
		View::share(compact('dashboard_url','viewsw','pete_options','system_vars','sidebar_options'));
		   
	}
	
	public function index()
	{
		
		$options = Option::where("visible",true)->orderBy('id', 'desc')->paginate(25);
		$current_user = Auth::user(); 
		return view('options.index', compact('options','current_user'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$option = new Option();
		$current_user = Auth::user(); 
		return view('options.create',compact('option','current_user'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		$option = new Option();

		$option->option_name = $request->input("option_name");
        $option->option_value = $request->input("option_value");
		$option->order = $request->input("order");
		$option->category = $request->input("category");
		$option->title = $request->input("title");
		$option->version = $request->input("version");
		$option->visible = true;
		$option->save();

		return redirect()->route('options.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$option = Option::findOrFail($id);
		$current_user = Auth::user(); 
		return view('options.show', compact('option'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
		$option = Option::findOrFail($id);
		$current_user = Auth::user(); 
		return view('options.edit', compact('option','current_user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$option = Option::findOrFail($id);

		$option->option_name = $request->input("option_name");
        $option->option_value = $request->input("option_value");
		$option->order = $request->input("order");
		$option->category = $request->input("category");
		$option->title = $request->input("title");
		$option->version = $request->input("version");
		
		$option->save();

		return redirect()->route('options.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$option = Option::findOrFail($id);
		$option->delete();

		return redirect()->route('options.index')->with('message', 'Item deleted successfully.');
	}

}
