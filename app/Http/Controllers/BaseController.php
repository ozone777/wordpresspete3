<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Log;
use App\PeteOption;
use App\User;
use Illuminate\Support\Facades\Auth;
use View;
use Illuminate\Support\Facades\Input;

class BaseController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function __construct(Request $request){
	    
	    $this->middleware('auth');
		$dashboard_url = env("DASHBOARD_URL");
		$viewsw = "/sites";
		
		//DEBUGING PARAMS
		$debug = env('DEBUG');
		if($debug == "active"){
			$inputs = $request->all();
			Log::info($inputs);
		}
		
		$system_vars = parent::__construct();
		$pete_options = $system_vars["pete_options"];
		$sidebar_options = $system_vars["sidebar_options"];
		$os_distribution = $system_vars["os_distribution"];
		$this->system_vars = $system_vars;
		
		View::share(compact('dashboard_url','viewsw','pete_options','system_vars','sidebar_options','os_distribution'));
		   
	}
	
	
	
	public function get_ads(){
		
		$dashboard_url = env("DASHBOARD_URL");
		
		$options = array(
		 
		  'http' => array(
		    'method'  => 'GET',
			'timeout' => 3000,
		    'header'=>  "Content-Type: application/json\r\n" .
		                "Accept: application/json\r\n"
		    )
		);
		
		$context  = stream_context_create( $options );
		$result = file_get_contents("$dashboard_url/ads_json", false, $context );
		$ads= json_decode( $result );
		
		return response()->json($ads);
		
	}
	
	public function premium_plugins(){
		
		$success = Input::get('success');
		$viewsw = "/premium_plugins";
		$dashboard_url = env("DASHBOARD_URL");
		$pete_environment = env("PETE_ENVIRONMENT");
		
		$options = array(
		 
		  'http' => array(
		    'method'  => 'GET',
			'timeout' => 3000,
		    'header'=>  "Content-Type: application/json\r\n" .
		                "Accept: application/json\r\n"
		    )
		);
		
		$context  = stream_context_create( $options );
		$result = file_get_contents("$dashboard_url/plugins_json", false, $context );
		$plugins = json_decode( $result );
		$pete_plugins = $plugins->pete_plugins;
		$wp_plugins = $plugins->wp_plugins;
		$current_user = Auth::user(); 
		
		return view('base.premium_plugins',compact('viewsw','pete_environment','pete_plugins','wp_plugins','dashboard_url','success','current_user'));
	}
	
	public function pete_update(){
		
		$mysql_v_dialog = $this->system_vars["mysql_v_dialog"];
		if(strpos($mysql_v_dialog, "mysql") !== false){
		     Log::info("MYSQL is active");
		} else{
			return Redirect::to('/premium_plugins')->withErrors(['msg' => 'Please restart WordPress Pete to solve the problem']);
		}
		
		$user = Auth::user();
		$pete_options = new PeteOption();
		$app_root = $pete_options->get_meta_value('app_root');
		$os = $pete_options->get_meta_value('os');
		$script = Input::get('script');
		
		//Load COMPOSER variables in Apache Server
	   	if($pete_options->get_meta_value('os_distribution') == "darwin"){
			Log::info("entro en darwin");
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer.phar");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
	   	}else if($pete_options->get_meta_value('os_distribution') == "ubuntu"){
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
	   	}
		
		//Remove break line characters
		//if($pete_environment != "development"){
			$script = str_replace("\r", "", $script);
			$script = str_replace("\n", "", $script);
			$script = str_replace("<CR>", "", $script);
			//}
		
		//Convert script in array
		$commands_array = explode(";", $script);
		
		//Run scripts
		$base_path = base_path();
		chdir("$base_path");
		$output = "";
		foreach ($commands_array as $command) {
			$command = escapeshellcmd($command);
			Log::info("command: ".$command);
			if($command != ""){
				$output .= shell_exec($command);
				Log::info($output);
			}
				
		}
		
		$user->output = $output;
		$user->save();
		
		return Redirect::to('/sites?update=true');
	}
	
	public function pete_plugins_install(){
		
		$mysql_v_dialog = $this->system_vars["mysql_v_dialog"];
		if(strpos($mysql_v_dialog, "mysql") !== false){
		     Log::info("MYSQL is active");
		} else{
			return Redirect::to('/premium_plugins')->withErrors(['msg' => 'Please restart WordPress Pete to solve the problem']);
		}
		
		$user = Auth::user();
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
		$script = Input::get('script');
		$redirect_after_install = Input::get('redirect_after_install');
		$pete_environment = env("PETE_ENVIRONMENT");
		
		//Load COMPOSER variables in Apache Server
	   	if($pete_options->get_meta_value('os_distribution') == "darwin"){
			Log::info("entro en darwin");
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer.phar");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
	   	}else if($pete_options->get_meta_value('os_distribution') == "ubuntu"){
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
	   	}
		
		$script = str_replace("\r", "", $script);
		$script = str_replace("\n", "", $script);
		$script = str_replace("<CR>", "", $script);
		
		$base_path = base_path();
		
		//Convert script in array
		$commands_array = explode(";", $script);
		
		//Run scripts
		chdir("$base_path");
		
		$output = "";
		foreach ($commands_array as $command) {
			$command = escapeshellcmd($command);
			Log::info("Executing command: ".$command);
			if($command != ""){
				$output = shell_exec($command);
				Log::info($output);
			}
				
		}
		
		$user->output = $output;
		$user->save();
		
		if(isset($redirect_after_install)){
			return Redirect::to($redirect_after_install);
		}else{
			return Redirect::to('/premium_plugins');
		}
		
	}
	
	public function pete_plugins_update(){
		
		$mysql_v_dialog = $this->system_vars["mysql_v_dialog"];
		if(strpos($mysql_v_dialog, "mysql") !== false){
		     Log::info("MYSQL is active");
		} else{
			return Redirect::to('/premium_plugins')->withErrors(['msg' => 'Please restart WordPress Pete to solve the problem']);
		}
		
		$user = Auth::user();
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
		$script = Input::get('script');
		$redirect_after_install = Input::get('redirect_after_install');
		$pete_environment = env("PETE_ENVIRONMENT");
		
		//Load COMPOSER variables in Apache Server
	   	if($pete_options->get_meta_value('os_distribution') == "darwin"){
			Log::info("entro en darwin");
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer.phar");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
	   	}else if($pete_options->get_meta_value('os_distribution') == "ubuntu"){
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
	   	}
		
		$script = str_replace("\r", "", $script);
		$script = str_replace("\n", "", $script);
		$script = str_replace("<CR>", "", $script);
		
		$base_path = base_path();
		
		//Convert script in array
		$commands_array = explode(";", $script);
		
		//Run scripts
		chdir("$base_path");
		
		$output = "";
		foreach ($commands_array as $command) {
			$command = escapeshellcmd($command);
			Log::info("Executing command: ".$command);
			if($command != ""){
				$output = shell_exec("$command 2>&1");
				Log::info($output);
			}
				
		}
		
		$user->output = $output;
		$user->save();
		
		if(isset($redirect_after_install)){
			return Redirect::to($redirect_after_install);
		}else{
			return Redirect::to('/premium_plugins');
		}
		
	}
	
	public function validate_pete(){
		
		$user = Auth::user();
		$reponse = User::validate_pete(Input::get('oemail'),Input::get('oapi_key'),Input::get('odomain'));
		return response()->json($reponse);
		
	}
	
	public function phpinfo_view(){
		
		return view('base.phpinfo_view');
		
	}

}
