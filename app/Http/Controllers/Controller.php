<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Log;
use App\PeteOption;
use App\Option;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	public function __construct(){
	
	  	$pete_options = new PeteOption();
	  	$sidebar_options = Option::where("category","sidebar")->orderBy("order","asc")->get();
		$os_distribution = $pete_options->get_meta_value('os_distribution');
		$os = $pete_options->get_meta_value('os');
		
		 if($pete_options->get_meta_value('os_distribution') == "darwin"){
			
			$apache_v_dialog=explode(' P',apache_get_version())[0];
			$mysql_v_dialog = shell_exec('mysql -V');
			
		}else if($pete_options->get_meta_value('os_distribution') == "ubuntu" ){
			///
			$apache_v_dialog = shell_exec('apache2 -v | grep version');
			$mysql_v_dialog = shell_exec('mysql -V');
			
		}else {
			$apache_v_dialog=explode(' P',apache_get_version())[0];
			$mysql_v_dialog=explode(', s',shell_exec('mysql -V'))[0];
		}
		
		$array = ["apache_v_dialog" => $apache_v_dialog, "mysql_v_dialog" => $mysql_v_dialog, "pete_options" => $pete_options, "sidebar_options" => $sidebar_options, "os_distribution" => $os_distribution, "os" => $os]; 
		return $array;
					
	}
}
