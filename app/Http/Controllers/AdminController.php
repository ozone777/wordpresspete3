<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Site;
use App\User;
use App\PeteOption;
use Illuminate\Http\Request;
use Input;
use Log;
use Illuminate\Support\Facades\Redirect;
use View;
use Illuminate\Support\Facades\Auth;
use Validator;

class AdminController extends Controller {

	public function __construct(Request $request)
	    {
		    $this->middleware('auth');
			$this->middleware('auth.admin');
			
			$dashboard_url = env("DASHBOARD_URL");
		
			//DEBUGING PARAMS
			$debug = env('DEBUG');
			if($debug == "active"){
				$inputs = $request->all();
				Log::info($inputs);
			}
		
			$system_vars = parent::__construct();
			$pete_options = $system_vars["pete_options"];
			$sidebar_options = $system_vars["sidebar_options"];
			$os_distribution = $system_vars["os_distribution"];
			$this->os_distribution = $os_distribution;
			$current_user = Auth::user(); 
			View::share(compact('dashboard_url','pete_options','system_vars','sidebar_options','current_user','os_distribution'));
			
	    }
		
	public function phpmyadmin_panel(){
		
		$root_pass = env('ROOT_PASS');
		$viewsw = "/phpmyadmin_panel";	
		
		if($this->os_distribution == "darwin"){
			$phpmyadmin_url = "http://phpmyadmin.test";
		}else if($this->os_distribution == "ubuntu"){
			$phpmyadmin_url = "/phpmyadmin";
		}
		
	 	return view('admin.phpmyadmin_panel',compact('root_pass','viewsw','phpmyadmin_url'));
	}
	
	public function phpinfo_panel(){
		
		$viewsw = "/phpinfo_panel";	
		$success = "";	
		$success = Input::get('success');	
	 	return view('admin.phpinfo_panel',compact('viewsw','success'));
	}
	
	public function change_php(){
		
		$php_version = Input::get('php_version');
		$debug = env('DEBUG');
		
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
		$os_distribution = $pete_options->get_meta_value('os_distribution');
		$os_version = $pete_options->get_meta_value('os_version');
		
		$command = "./unix_change_php.sh -v {$php_version} -k {$debug}";
		Log::info($command);
		
  	  	$base_path = base_path();
    	chdir("$base_path/scripts/");
		
		if($this->os_distribution == "ubuntu"){
			$command = "./unix_change_php.sh -v {$php_version} -d {$os_distribution} -e {$os_version} -k {$debug}";
			$output = shell_exec($command);
			Log::info("Output:");
			Log::info($output);
		}
		return response()->json(['ok' => 'OK']);
	}
	
	public function list_users(){
		
		$users = User::orderBy('id', 'desc')->paginate(10);
		$viewsw = "/list_users";		
	    return view('admin.list_users',compact('users','viewsw'));
		
	}
	
	 public function create_user()
	 {
		 $viewsw = "/list_users";		
		 $user = new User();
		 return view('admin.create_user', compact('user','viewsw'));
	}
	
	public function store_user(Request $request){
		
		$name = Input::get('name');
		$email = Input::get('email');
		$password = Input::get('password');
		
		$fields_to_validator = $request->all();
		
    	$validator = Validator::make($fields_to_validator, [
	   	 'name' =>  array('required', 'regex:/^[a-zA-Z0-9-_]+$/','unique:users'),
       	 'email' => 'required|unique:users',
    	 ]);
		 
      	if ($validator->fails()) {
			
 	    	return redirect('create_user')
 	    		->withErrors($validator)
 	    			->withInput();
 			
      	 }
		
        User::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
        ]);
		
		return Redirect::to('/list_users');
		
	}
	
	 public function edit_user()
	{
		$viewsw = "/list_users";		
		$user = User::findOrFail(Input::get('id'));
		return view('admin.edit_user', compact('user','viewsw'));
	}
	
	public function update_user(){
		
		$user = User::findOrFail(Input::get('user_id'));
		$email = Input::get('email');
		$name = Input::get('name');
		$password = Input::get('password');
		
		if(isset($email))
			$user->email = Input::get('email');
		
		if(isset($name))
			$user->name = Input::get('name');
		
		if(isset($password))
			$user->password = bcrypt($password);
		
		$user->save();
		return Redirect::to('/list_users');

	}
	
	 public function delete_user()
	 {
		 $user = User::findOrFail(Input::get('user_id'));
		 $user->delete();
		 return Redirect::back();
	}
	
	

}
