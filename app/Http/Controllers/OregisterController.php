<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Option;
use App\Http\Controllers\Controller;
use Log;
use Input;
use App\PeteOption;
use Illuminate\Routing\Route;
use View;
use Illuminate\Support\Facades\Auth;

class OregisterController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
	
	public function __construct(Request $request){
	    
	    $this->middleware('auth');
		$dashboard_url = env("DASHBOARD_URL");
		$viewsw = "/sites";
		
		//DEBUGING PARAMS
		$debug = env('DEBUG');
		if($debug == "active"){
			$inputs = $request->all();
			Log::info($inputs);
		}
		
		$system_vars = parent::__construct();
		$pete_options = $system_vars["pete_options"];
		$sidebar_options = $system_vars["sidebar_options"];
		$current_user = Auth::user(); 
		View::share(compact('dashboard_url','viewsw','pete_options','system_vars','sidebar_options','current_user'));
		   
	}
	
	
	public function update_license(){
		
			Log::info('Entro en update license');
			
			if(input::get('created_at_string')){
				
				Option::delete_meta_value('created_at_string');
				
				$option = new Option();
				$option->option_name = "created_at_string";
		        $option->option_value = input::get('created_at_string');
				$option->visible = false;
				$option->save();
			}
			
			if(input::get('token')){
				
				Option::delete_meta_value('token');
				
				$option = new Option();
				$option->option_name = "token";
		        $option->option_value = input::get('token');
				$option->visible = false;
				$option->save();
			}
			
			if(input::get('validation_user_email')){
				
				Option::delete_meta_value('validation_user_email');
				
				$option = new Option();
				$option->option_name = "validation_user_email";
		        $option->option_value = input::get('validation_user_email');
				$option->visible = false;
				$option->save();
			}
			
			if(input::get('validation_api_key')){
				
				Option::delete_meta_value('validation_api_key');
				
				$option = new Option();
				$option->option_name = "validation_api_key";
		        $option->option_value = input::get('validation_api_key');
				$option->visible = false;
				$option->save();
				
				Option::delete_meta_value('validation_event');
				
				$option = new Option();
				$option->option_name = "validation_event";
				$option->option_value = "validation_event";
				$option->option_date = date('Y-m-d H:i:s',strtotime('+1 year'));
				$option->visible = false;
				$option->save();
			}
			
	  	  	return response()->json($option);
	}
	
    public function register_your_license()
    {
		$viewsw = "sites";
		$pete_options = new PeteOption();
		$validation_url = $pete_options->get_meta_value('validation_url');
		return view('oregisters.register_your_license', compact('validation_url','pete_options','viewsw'));
    }
}