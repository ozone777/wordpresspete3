<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;
use App\Secure;
use DB;
use App\Pete;
use App\Backup;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\PeteOption;
use Crypt;

class Site extends Model {
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];
	
	public function user()
	{
	    return $this->belongsTo('App\User');
	}
		
	public static function reload_server() {
		
		$output = "";
		$pete_options = new PeteOption();	
		$os_distribution = $pete_options->get_meta_value('os_distribution');
		$base_path = base_path();
		$debug = env('DEBUG');
		
		chdir("$base_path/scripts/");
		
   	   	if ($os_distribution == "darwin"){
		 	$output = shell_exec('sudo /usr/local/opt/httpd/bin/httpd -k graceful'); 
	   	}else if($os_distribution =="ubuntu"){
			$output = shell_exec("sudo /etc/init.d/apache2 reload");
	   	}else {
			switch (PHP_VERSION) {
				case "5.6.38":
					$output=shell_exec('restart.cmd -n Aphp5_6'); 
				break;
				case "7.0.32":
					$output=shell_exec('restart.cmd -n Aphp7_0'); 
				break;
				case "7.1.24":
					$output=shell_exec('restart.cmd -n Aphp7_1'); 
				break;
				case "7.2.12":
					$output=shell_exec('restart.cmd -n Aphp7_2'); 
				break;
			}
		}
		
		if($debug == "active")
			Log::info("Reload server: $output");
		return $output;
	}
	
	
	public function create_ssl_conf($pete_options,$ssl_crt,$ssl_key,$ssl_bundle){
		
    	$app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
    	$server_conf = $pete_options->get_meta_value('server_conf');
		$debug = env('DEBUG');
		$os_version = $pete_options->get_meta_value('os_version');
		$os_distribution = $pete_options->get_meta_value('os_distribution');
		$os = $pete_options->get_meta_value('os');
		
  	 	$base_path = base_path();
    	chdir("$base_path/scripts/");
		
		if($os_distribution=="ubuntu") {
					
			$command="./{$os}_create_ssl.sh -n {$this->name} -u {$this->url} -a {$server_conf} -r {$app_root} -t {$ssl_crt} -k {$ssl_key} -b {$ssl_bundle}";	
			$output = shell_exec($command);
		}
		
  	  	if($debug == "active"){
  			Log::info('Pete create_ssl_conf: ' . $command);
			$this->output = $this->output . "#######create_ssl_conf#######\n";
  	  	}
		
		$this->output .= $output;
		$this->ssl = true;
		$this->save();
	}
	
	
    public function find_wp_config_path()
    {
        //limit nr of iterations to 20
        $i = 0;
        $maxiterations = 20;
       // $dir = dirname(__FILE__);
 	  	$pete_options = new PeteOption();
 	  	$app_root = $pete_options->get_meta_value('app_root');
 	 	$dir = $app_root . "/" . $this->name;
		
        do {
            $i++;
            if (file_exists($dir . "/wp-config.php")) {
                return $dir . "/wp-config.php";
            }
        } while (($dir = realpath("$dir/..")) && ($i < $maxiterations));
        return null;
    }
	
    public function remove_ssl_from_siteurl_in_wpconfig()
    {	
        $wpconfig_path = $this->find_wp_config_path();
        if (!empty($wpconfig_path)) {
            $wpconfig = file_get_contents($wpconfig_path);

            $homeurl_pos = strpos($wpconfig, "define('WP_HOME','https://");
            $siteurl_pos = strpos($wpconfig, "define('WP_SITEURL','https://");

            if (($homeurl_pos !== false) || ($siteurl_pos !== false)) {
                if (is_writable($wpconfig_path)) {
                    $search_array = array("define('WP_HOME','https://", "define('WP_SITEURL','https://");
                    $ssl_array = array("define('WP_HOME','http://", "define('WP_SITEURL','http://");
                    //now replace these urls
                    $wpconfig = str_replace($search_array, $ssl_array, $wpconfig);
                    file_put_contents($wpconfig_path, $wpconfig);
                } else {
                    $this->errors['wpconfig not writable'] = TRUE;
                }
            }

        }
    }
	
	
	public function delete_ssl() {
		
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
	    $app_root = $pete_options->get_meta_value('app_root');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		
		$logs_route = $pete_options->get_meta_value('logs_route');
		$os_distribution = $pete_options->get_meta_value('os_distribution');
				
		$app_name = $this->app_name ;
		$id = $this->id;
		$project_url = $this->url;	
        $project_name = $this->name;
		
		$debug = env('DEBUG');
		
		$base_path = base_path();
		chdir("$base_path/scripts/");
		$command ="";
		if($os_distribution=="ubuntu") {
			
			$this->remove_ssl_from_siteurl_in_wpconfig();
			
			$command = "./{$os}_delete_ssl.sh -n {$project_name} -r {$app_root} -a {$server_conf} -v {$server} -w {$server_version} -u {$project_url} -o {$os_version} -j {$os_version} -m {$logs_route} -z {$os_distribution} -k {$debug}";
			
			
			$output = shell_exec($command);
	  	  	if($debug == "active"){
				Log::info("Action: continue_wordpress");
	  			Log::info($command);
		  		Log::info("Output:");
				Log::info($output);
			}
			
			$this->output = $this->output . "#######delete_ssl#######\n";
			$this->output = $output;	
			$this->suspend = false;
			$this->save();
	  
		}
		
	}
	
	public function normalize_name(){
		if (strpos($this->name, '_') !== false) {
			$aux = $this->name;
			$pos = strpos($aux, "_");
			$str1 = substr($aux,0,$pos+1);
			$this->name = str_replace($str1,"",$aux);
		}
	}
	
	public function snapshot_creation($label){
		
  	  $pete_options = new PeteOption();
  	  $app_root = $pete_options->get_meta_value('app_root');
      $mysql_bin = $pete_options->get_meta_value('mysql_bin');
  	  $server_conf = $pete_options->get_meta_value('server_conf');
  	  $os = $pete_options->get_meta_value('os');
  	  $server = $pete_options->get_meta_value('server');
  	  $server_version = $pete_options->get_meta_value('server_version');
  	  $this->wp_load_path = $app_root . "/" . $this->name;
	  $table_prefix = "wp_";
	  
  	  $mysqldump = $mysql_bin . "mysqldump";
  	  $id = $this->id;
	  
  	  $db_root_pass = env('ROOT_PASS');
  	  $debug = env('DEBUG');
	  
	  $base_path = base_path();
  	  chdir("$base_path/scripts/");
	  
  	  
	  //SET BACKUP////////
	  ////////////////////
	  $backup = new Backup();
	  $backup->site_id = $this->id;
	  $backup->theme = $this->theme;
	  $backup->first_password = $this->first_password;
	  $backup->wp_user = $this->wp_user;
	  $backup->name = $this->name;
	  $backup->url =  $this->url;
	  $backup->schedulling = $label;
	  $backup->file_name = $this->name."-".$backup->schedulling.".tar.gz";
	  $backup->manual = true;
	  $backup->user_id = $this->user_id;
	  $backup->barserver_id = $this->barserver_id;  
	  
	  $command="./backup.sh -n {$this->name} -p {$db_root_pass} -r {$app_root} -q {$base_path} -m {$mysqldump} -v {$os} -w {$this->app_name} -u {$this->url} -d {$this->id} -s {$backup->schedulling} -b {$this->db_name} -f {$backup->file_name} -k {$debug}";	
	$output = shell_exec($command);
	  
	  $backup->save();
	  ///////////////////
	  ///////////////////
	  
	  $output = shell_exec($command);
  	  if($debug == "active"){
		Log::info("Action: snapshot_creation");
  		Log::info($command);
  		Log::info("Output:");
		Log::info($output);
  	  }
	  
  	  return $backup;
  	}
	
	
	public function force_delete_wordpress() {
			
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		$os_version = $pete_options->get_meta_value('os_version');
		$os_distribution = $pete_options->get_meta_value('os_distribution');
		$mysqlcommand = $mysql_bin . "mysql";
		$app_name = $this->app_name ;
		$project_name = str_replace("_odeleted_$this->id","",$this->name);
			
		$debug = env('DEBUG');
		$db_root_pass = env('ROOT_PASS');
		
		$base_path = base_path();
		chdir("$base_path/scripts/");
		
		if ($os=="Windows_NT") {
			$command = "{$os}_force_delete.cmd -n {$project_name} -r {$app_root} -a {$server_conf} -v {$os} -j {$os_version} -s {$this->id} -l {$server} -w {$server_version} -t {$app_name} -k {$debug}";
		}else {
			$command = "./{$os}_force_delete.sh -n {$project_name} -r {$app_root} -q {$base_path} -a {$server_conf} -v {$os} -j {$os_version} -s {$this->id} -l {$server} -w {$server_version} -p {$db_root_pass} -x {$this->db_name} -t {$app_name} -z {$os_distribution} -k {$debug}";
		}
		
		$output = shell_exec($command);
  	  	if($debug == "active"){
			Log::info("Action: force_delete_wordpress");
  			Log::info($command);
	  		Log::info("Output:");
			Log::info($output);
  	  	}
		
		$this->output = $this->output . "#######FORCE DELETE#######\n";
		$this->output = $this->output . $output;
		
		$this->save();
	}
		
	public function suspend_wordpress() {
		
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$app_root = $pete_options->get_meta_value('app_root');	
		$os_distribution = $pete_options->get_meta_value('os_distribution');	
		$project_name = $this->name;
		$app_name = $this->app_name ;
		$id = $this->id;
		$project_url = $this->url;
		
		$debug = env('DEBUG');
		$db_root_pass = env('ROOT_PASS');
		
		$base_path = base_path();
		chdir("$base_path/scripts/");

		if ($os=="Windows_NT") {
			$command = "{$os}_suspend.cmd -n {$project_name} -r {$app_root} -a {$server_conf} -v {$server} -w {$server_version } -u {$project_url} -o {$os_version} -j {$os_version} -k {$debug}";
		}else {
			$command = "./{$os}_suspend.sh -n {$project_name} -r {$app_root} -q {$base_path} -a {$server_conf} -v {$server} -w {$server_version } -u {$project_url} -o {$os_version} -j {$os_version} -p {$os_distribution} -k {$debug}";
		}

		$output = shell_exec($command);
  	  	if($debug == "active"){
			Log::info("Action: suspend_wordpress");
  			Log::info($command);
	  		Log::info("Output:");
			Log::info($output);
  	  	}

		$this->output = $this->output . "#######SUSPEND#######\n";
		$this->output = $output;
		$this->suspend = true;
		$this->save();
	}
	
	public function continue_wordpress() {
		
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
	    $app_root = $pete_options->get_meta_value('app_root');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		
		$logs_route = $pete_options->get_meta_value('logs_route');
		$os_distribution = $pete_options->get_meta_value('os_distribution');
				
		$app_name = $this->app_name ;
		$id = $this->id;
		$project_url = $this->url;	
        $project_name = $this->name;
		
		$debug = env('DEBUG');
		
		$base_path = base_path();
		chdir("$base_path/scripts/");

		if ($os=="Windows_NT") {
			$command = "{$os}_continue.cmd -n {$project_name} -r {$app_root} -a {$server_conf} -v {$server} -w {$server_version} -u {$project_url} -o {$os_version} -j {$os_version} -k {$debug}";
		}else {
			$command = "./{$os}_continue.sh -n {$project_name} -r {$app_root} -a {$server_conf} -v {$server} -w {$server_version} -u {$project_url} -o {$os_version} -j {$os_version} -m {$logs_route} -z {$os_distribution} -k {$debug}";
		}
		
		$output = shell_exec($command);
  	  	if($debug == "active"){
			Log::info("Action: continue_wordpress");
  			Log::info($command);
	  		Log::info("Output:");
			Log::info($output);
  	  	}
		
		$this->output = $this->output . "#######CONTINUE#######\n";
		$this->output = $output;	
		$this->suspend = false;
		$this->save();
	}
	
	public function mod_wordpress($mod_sw) {
		
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
	    $app_root = $pete_options->get_meta_value('app_root');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		
		$logs_route = $pete_options->get_meta_value('logs_route');
		$os_distribution = $pete_options->get_meta_value('os_distribution');
				
		$app_name = $this->app_name ;
		$id = $this->id;
		$project_url = $this->url;	
        $project_name = $this->name;
		$this->mod_sw = $mod_sw;
		
		$debug = env('DEBUG');
		
		$base_path = base_path();
		chdir("$base_path/scripts/");

		if ($os=="Windows_NT") {
			$command = "{$os}_mod.cmd -n {$project_name} -r {$app_root} -a {$server_conf} -v {$server} -w {$server_version} -u {$project_url} -o {$os_version} -j {$os_version} -k {$debug}";
		}else {
			$command = "./{$os}_mod.sh -n {$project_name} -r {$app_root} -a {$server_conf} -v {$server} -w {$server_version} -u {$project_url} -o {$os_version} -m {$logs_route} -z {$os_distribution} -s {$mod_sw} -k {$debug}";
		}
		
		$output = shell_exec($command);
  	  	if($debug == "active"){
			Log::info("Action: mod_wordpress");
  			Log::info($command);
	  		Log::info("Output:");
			Log::info($output);
  	  	}
		$this->save();
	}
	
	public function delete_wordpress() {
			
		$pete_options = new PeteOption();
		$os = $pete_options->get_meta_value('os');
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		$os_version = $pete_options->get_meta_value('os_version');
		$os_distribution = $pete_options->get_meta_value('os_distribution');
		
		$mysqlcommand = $mysql_bin . "mysql";
		$app_name = $this->app_name ;
		$id = $this->id;
		$project_name = $this->name;
			
		$debug = env('DEBUG');
		
		$base_path = base_path();
		chdir("$base_path/scripts/");
			
		if ($os=="Windows_NT") {
			$command = "{$os}_delete.cmd -n {$project_name} -r {$app_root} -a {$server_conf} -v {$os} -s {$id} -j {$os_version} -l {$server} -w {$server_version} -k {$debug} -url {$this->url}";
		}else {
			$command = "./{$os}_delete.sh -n {$project_name} -r {$app_root} -q {$base_path} -a {$server_conf} -v {$os} -s {$id} -j {$os_version} -l {$server} -w {$server_version} -p {$os_distribution} -k {$debug}";
		}

		$output = shell_exec($command);
  	  	if($debug == "active"){
			Log::info("Action: delete_wordpress");
  			Log::info($command);
	  		Log::info("Output:");
			Log::info($output);
  	  	}
		
		$this->output = $this->output . "#######DELETE#######\n";
		$this->output = $output;
		$this->url = $this->url . "_odeleted_" . $this->id;
		$this->name = $this->name . "_odeleted_" . $this->id;
		
		$this->save();
	}
	
	public function restore_wordpress() {
		
		$this->url = str_replace("_odeleted_$this->id","",$this->url);
		$this->name = str_replace("_odeleted_$this->id","",$this->name);
		
		$site_url = $this->url;	
        $site_name = $this->name;
		
		$pete_options = new PeteOption();
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$os_version = $pete_options->get_meta_value('os_version');
  	    $os = $pete_options->get_meta_value('os');
		
		$logs_route = $pete_options->get_meta_value('logs_route');
		$os_distribution = $pete_options->get_meta_value('os_distribution');
		
		$mysqlcommand = $mysql_bin . "mysql";
		$app_name = $this->app_name ;
		$id = $this->id;
		$db_root_pass = env('ROOT_PASS');
		$debug = env('DEBUG');
		
		$base_path = base_path();
		chdir("$base_path/scripts/");
	
		if ($os=="Windows_NT") {
			$command = "{$os}_restore.cmd -n {$site_name} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -v {$server} -w {$server_version} -s {$this->id} -u {$this->url} -j {$os_version} -k {$debug}";
		}else {
			$command = "./{$os}_restore.sh -n {$site_name} -p {$db_root_pass} -r {$app_root} -q {$base_path} -m {$logs_route} -z {$os_distribution} -a {$server_conf} -v {$server} -w {$server_version} -s {$this->id} -u {$this->url} -j {$os_version} -h {$app_name} -k {$debug}";
		}
		
  	   
		$output = shell_exec($command);
  	  	if($debug == "active"){
			Log::info("Action: restore_wordpress");
  			Log::info($command);
	  		Log::info("Output:");
			Log::info($output);
  	  	}
		
		$this->output = $this->output . "#######RESTORE#######\n";
		$this->output = $this->output . $output;
		$this->save();
		
	}
	
	public function export_wordpress() {

	  $pete_options = new PeteOption();
	  $app_root = $pete_options->get_meta_value('app_root');
      $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	  $server_conf = $pete_options->get_meta_value('server_conf');
	  $os = $pete_options->get_meta_value('os');
	  $server = $pete_options->get_meta_value('server');
	  $server_version = $pete_options->get_meta_value('server_version');
	  $this->wp_load_path = $app_root . "/" . $this->name;
	  $table_prefix = "wp_";
	  $mysqldump = $mysql_bin . "mysqldump";
	  $id = $this->id;
	  
	  $db_root_pass = env('ROOT_PASS');
	  $debug = env('DEBUG');
	  
	  $base_path = base_path();
	  chdir("$base_path/scripts/");
	  if ($os=="Windows_NT") {
		  
		  $this->wp_load_path = $app_root . "\\" . $this->name;
		  require("$this->wp_load_path\wp-config.php");
		  $this->DB_NAME = constant('DB_NAME');
		  
		  $command = "{$os}_export.cmd -n {$this->name} -p {$db_root_pass} -r {$app_root} -m {$mysqldump} -v {$os} -w {$server} -t {$server_version} -u {$this->url} -a {$this->app_name} -k {$debug} -odb {$this->DB_NAME} -pr {$table_prefix}";
	  }else {
		  		  
		  $command = "./{$os}_export.sh -n {$this->name} -p {$db_root_pass} -r {$app_root} -z {$base_path} -m {$mysqldump} -v {$os} -w {$server} -t {$server_version} -u {$this->url} -a {$this->app_name} -q {$this->db_name} -i {$table_prefix} -k {$debug}";
	  }
	  
		$output = shell_exec($command);
	  	if($debug == "active"){
			Log::info("Action: export_wordpress");
			Log::info($command);
  			Log::info("Output:");
			Log::info($output);
	  	}
		
		$this->output = $this->output . "#######EXPORT#######\n";
		$this->output = $output;
	  
	  if($os == "unix"){
		$this->file_to_download = "export/{$this->name}.tar.gz";
	  }else{
	  	$this->file_to_download = "export/{$this->name}.zip";
	  }
	  
	  $this->save();
	}
	
	public function new_wordpress() {
		
		$pete_options = new PeteOption();
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		$logs_route = $pete_options->get_meta_value('logs_route');
		$os_distribution = $pete_options->get_meta_value('os_distribution');
		
	    $db_name = "db_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		$db_user = "usr_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		$db_user_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
 		
		$this->db_name = $db_name;
		$this->mod_sw = "on";
		
	    $wpkey1 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey2 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey3 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey4 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey5 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey6 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey7 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey8 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
		
		$db_root_pass = env('ROOT_PASS');
		$mysqlcommand = $mysql_bin . "mysql";
		$debug = env('DEBUG');
		
		$base_path = base_path();
		chdir("$base_path/scripts/");
		
		if ($os=="Windows_NT") {
			$command = "{$os}_new.cmd -n {$this->name} -u {$this->url} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -x {$db_name} -y {$db_user} -z {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -j {$os_version} -t {$server} -w {$server_version} -k {$debug} ";
		}else {
			$command = "./{$os}_new.sh -n {$this->name} -u {$this->url} -p {$db_root_pass} -r {$app_root} -q {$base_path} -m {$logs_route} -s {$os_distribution} -a {$server_conf} -x {$db_name} -y {$db_user} -z {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -j {$os_version} -t {$server} -w {$server_version} -k {$debug} ";
		}
		
		
		$output = shell_exec($command);
	  	if($debug == "active"){
			Log::info("Action: new_wordpress");
			Log::info($command);
  			Log::info("Output:");
			Log::info($output);
	  	}

		$this->output = $this->output . "#######NEW WORDPRESS#######\n";
		$this->output = $output;
	   
	   $this->app_name = "WordPress";
	   $this->output = $this->output . shell_exec($command);
	   $this->save();
		
	}
	
	public function import_wordpress($template_url="none",$database_variables=null) {
		
		$pete_options = new PeteOption();
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		$apache_version = $pete_options->get_meta_value('apache_version');
		$logs_route = $pete_options->get_meta_value('logs_route');
		$os_distribution = $pete_options->get_meta_value('os_distribution');
		
		if(isset($database_variables)){
			$db_name = $database_variables["db_name"];
			$db_user = $database_variables["db_user"];
			$db_user_pass = $database_variables["db_user_pass"];
			/*
			Log::info("entro en database_variables");
			Log::info("db_name: ".$db_name);
			Log::info("db_user: ".$db_user);
			Log::info("db_user_pass: ".$db_user_pass);
			*/
		}else{
		    $db_name = "db_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		    $db_user = "usr_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		    $db_user_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		}
		$this->db_name = $db_name;
		
	    $wpkey1 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey2 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey3 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey4 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey5 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey6 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey7 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
	    $wpkey8 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 66);
		
		$db_root_pass = env('ROOT_PASS');
		$mysqlcommand = $mysql_bin . "mysql";
		$debug = env('DEBUG');
		
		$base_path = base_path();
		chdir("$base_path/scripts/");
		
		//TEMPLATE CASES
		if($template_url =="none"){
			if($this->big_file_route != ""){
				$file_route = $this->big_file_route;
			}else{
				$file_route = "$base_path/public/uploads/$this->zip_file_url";
			}
			$type="none";
		}else if(strpos($template_url, 'http') !== false) {
		   $type = "url";
		   $file_route = $template_url;
		}else{
		   $type = "file";
		   $file_route = $template_url;
		} 

		if ($os=="Windows_NT") {
			$command = "{$os}_import.cmd -n {$this->name} -u {$this->url} -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -x {$db_name} -y {$db_user} -l {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -j {$os_version} -t {$server} -w {$server_version} -o {$file_route} -s {$type} -k {$debug}" ;
		}else {
			$command = "./{$os}_import.sh -n {$this->name} -u {$this->url} -p {$db_root_pass} -r {$app_root} -q {$base_path} -m {$logs_route} -a {$server_conf} -x {$db_name} -y {$db_user} -l {$db_user_pass} -b {$wpkey1} -c {$wpkey2} -d {$wpkey3} -e {$wpkey4} -f {$wpkey5} -g {$wpkey6} -h {$wpkey7} -i {$wpkey8} -v {$os} -j {$os_version} -t {$server} -w {$server_version} -o {$file_route} -s {$type} -z {$os_distribution} -k {$debug}" ;
		}	
		
		
		$output = shell_exec($command);
	  	if($debug == "active"){
			Log::info("Action: import_wordpress");
			Log::info($command);
  			Log::info("Output:");
			Log::info($output);
	  	}
		$this->mod_sw = "on";
		$this->output = $this->output . "####### IMPORT WORDPRESS #######\n";	 
		$this->output = $output;
		$this->app_name = "WordPress"; 
		$this->save();
		
	}	
	
	public function wordpress_laravel() {
		
		$pete_options = new PeteOption();
	    $app_root = $pete_options->get_meta_value('app_root');
        $mysql_bin = $pete_options->get_meta_value('mysql_bin');
	    $server_conf = $pete_options->get_meta_value('server_conf');
		$os = $pete_options->get_meta_value('os');
		$os_version = $pete_options->get_meta_value('os_version');
		$server = $pete_options->get_meta_value('server');
		$server_version = $pete_options->get_meta_value('server_version');
		$apache_version = $pete_options->get_meta_value('apache_version');
		
		$logs_route = $pete_options->get_meta_value('logs_route');
		$os_distribution = $pete_options->get_meta_value('os_distribution');
		
		$db_root_pass = env('ROOT_PASS');
		$mysqlcommand = $mysql_bin . "mysql";
		$debug = env('DEBUG');


		$base_path = base_path();
		
		
		$target_site = Site::findOrFail($this->wordpress_laravel_target_id);
		$this->wordpress_laravel_url = $this->wordpress_laravel_name . '.' . $target_site->url;
		$this->url = $this->wordpress_laravel_url;	
	    $this->app_name = "WordPress+Laravel";
		$this->wp_load_path = $app_root . "/" . $target_site->name;
		$this->wp_url = $target_site->url;
		
		if($this->action_name == "new_wordpress_laravel"){
			$this->action_name = "New";
		}else{
			$this->action_name = "Import";
		}
		
		if((!$this->wordpress_laravel_git) || ($this->wordpress_laravel_git == "")){
	  		$this->wordpress_laravel_git_branch = "master";
	  		$this->wordpress_laravel_git = "https://github.com/peterconsuegra/wordpresspluslaravel.git";
		}
	  
		require("$this->wp_load_path/wp-config.php");
		$this->DB_USER = constant('DB_USER');
		$this->DB_NAME = constant('DB_NAME');
		$this->DB_PASSWORD = constant('DB_PASSWORD');

		if ($os=="Windows_NT") {
			$command = "{$os}_wordpress_laravel.cmd -p {$db_root_pass} -r {$app_root} -m {$mysqlcommand} -a {$server_conf} -b {$this->wordpress_laravel_git_branch} -g {$this->wordpress_laravel_git} -n {$this->name} -u {$this->wordpress_laravel_url} -j {$os_version} -v {$os} -l {$this->wp_load_path} -e {$this->wp_url} -t {$server} -w {$server_version} -c {$this->action_name} -o {$this->laravel_version} -k {$debug} -odb {$this->DB_NAME} -udb {$this->DB_USER} -pdb {$this->DB_PASSWORD}";
	  	}else {
			#hack project_name for multiple dashboard.* logic
			$this->name = $this->name . str_replace(".","",$this->wp_url);
			
			if($os_distribution == "darwin"){
				$composer_bin = "composer.phar";
			}else if($os_distribution == "ubuntu"){
				$composer_bin = "composer";
			}
			
			$command = "./{$os}_wordpress_laravel.sh -p {$db_root_pass} -r {$app_root} -m {$logs_route} -z {$os_distribution} -a {$server_conf} -b {$this->wordpress_laravel_git_branch} -g {$this->wordpress_laravel_git} -n {$this->name} -u {$this->wordpress_laravel_url} -j {$os_version} -v {$os} -l {$this->wp_load_path} -e {$this->wp_url} -t {$server} -w {$server_version} -c {$this->action_name} -o {$this->laravel_version} -h {$composer_bin} -k {$debug} -q {$this->DB_NAME} -y {$this->DB_USER} -i {$this->DB_PASSWORD}";
		}	
	   
	    chdir("$base_path/scripts/");
	
		putenv("DB_DATABASE=".DB_NAME);
		putenv("DB_USERNAME=".DB_USER);
		putenv("DB_PASSWORD=".DB_PASSWORD);
		
	   	if($os_distribution == "darwin"){
			
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer.phar");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
			
	   	}else if($os_distribution == "ubuntu"){
	   		putenv("COMPOSER_HOME=/usr/local/bin/composer");
			putenv("COMPOSER_CACHE_DIR=~/.composer/cache");
	   	}
		
		$output = shell_exec($command);
	  	if($debug == "active"){
			Log::info("Action: import_wordpress");
			Log::info($command);
  			Log::info("Output:");
			Log::info($output);
	  	}
		
		$this->output = $this->output . "####### WORDPRESS LARAVEL #######\n";	 
		$this->output .= $output;
	   	$this->save();
	  
	}
	
}
