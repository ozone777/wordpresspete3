@extends('layout')

@section('header')
  
@endsection

@section('content')


		
        <div class="row">


            <div class="col-md-12">

				<h3>Register your License</h3>
				
				<p>To install the premium plugins of Wordpress Pete you need to buy the full version at: <a href="https://wordpresspete.com/subscriptions/">https://wordpresspete.com/subscriptions</a></p>
				
			</div>
			
		</div>
		
			
        <div class="row">


            <div class="col-md-8">			
					      
				  Registered email: <input type="text" id="oemail" name="oemail" class="form-control" value ="{{$pete_options->get_meta_value('validation_user_email')}}"><br>
					

            </div>
     </div>
	 
     <div class="row">
         <div class="col-md-8">
			 
			 API Key <input type="password" id="oapi_key" name="oapi_key" class="form-control" value ="{{$pete_options->get_meta_value('validation_api_key')}}"><br>
			 
          </div>
     </div>


	  <input type="submit" id= "validate_pete" class="btnpete" value="Submit">
	  
	  <br />
		 
		 <script>
	
		 	$("#validate_pete").click(function() {
		
		 		oemail = $("#oemail").val();
		 		oapi_key = $("#oapi_key").val();
				odomain = "{{str_replace("/register_your_license","",Request::url())}}";
				
				activate_loader();
				
		 		$.ajax({
		 			url: "/validate_pete",
		 			dataType: 'JSON',
		 			type: 'POST',
		 			data: {oemail : oemail, oapi_key: oapi_key, odomain: odomain},
		 			success : function(result) {
						
		 				if(result["message"]){

		 					$.notify({
		 						icon: "",
		 						message: result["message"]

		 					},{
		 						type: 'info',
		 						timer: 4000
		 					});
		 					$("#loadMe").modal("hide");
		 					deactivate_loader();
		 				    return false;	
					
		 			  	}else{
							deactivate_loader();
		 					window.location.href = "/sites";
		 			  	}
				
		 			}		
		 		});		
					  
	    
		 	});		
	
		 </script>
		 
		 
		 
@endsection



