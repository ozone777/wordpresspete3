<script type="text/javascript">
	
	$(".btnpete").click(function() {
  	  	 activate_loader();
	});
	
		function load_banner(){
	     $.ajax({
	           url: "/get_ads",
	           type: "GET",
	           datatype: 'json',
	           success: function(data){
	            
				html = '<ul class="bannerList">';
			 	for (var item in data) {
					html += '<li><a href="'+data[item].url+'"><img src="{{$dashboard_url}}/ads_images/'+data[item].image+'" /></a></li>';
				}
				html += '</ul>';
				$(".bannerListWpr").html(html);
				$('.simpleBanner').simplebanner({autoRotate: true, rotateTimeout: 3000});
	           }
	
	     });
		}
		
		function check_pete_version(){
			var pete_version = {{$pete_options->get_meta_value('version')}};
			pete_version = parseFloat(pete_version);
			console.log(pete_version);
			
		 	$.ajax({
		 		url: "{{$dashboard_url}}/update_button_json",
		 		dataType: 'JSONP',
		 		type: 'GET',
				data: {parent_version : "{{$pete_options->get_meta_value('parent_version')}}"},
		 		success : function(result) {
					
					last_version = parseFloat(result.version)
					console.log("last_version");
					console.log(last_version);
					
					html="";
					
					if(pete_version < last_version){
					
					<?php if($os_distribution == "darwin"){ ?>
					
					html+='<form class="form-inline" action="/pete_update" method="POST">';
       			 	html+='<input type="hidden" name="script" value="'+result.mac_script+'">';
					html+='<button class="update_pete_button" type="submit">Update</button>';
					html+='</form>';
					
					
					<?php }else if($os_distribution == "ubuntu"){ ?>
					
					html+='<form class="form-inline" action="/pete_update" method="POST">';
       			 	html+='<input type="hidden" name="script" value="'+result.olinux_script+'">';
					html+='<button class="update_pete_button" type="submit">Update</button>';
					html+='</form>';
					
					<?php }else if($os_distribution == "win"){ ?>
					
					html+='<form class="form-inline" action="/pete_update" method="POST">';
       			 	html+='<input type="hidden" name="script" value="'+result.win_script+'">';
					html+='<button class="update_pete_button" type="submit">Update</button>';
					html+='</form>';
					
					<?php } ?>						
					
					}
					
					//$("#update_pete").html('<a class="btnpeteupdate" href="/auth/logout">Update</a>');
					$("#update_pete").html(html);
	           }
	
	     	});
		}
		
		
		$(".clone_action").click(function() {
			site_id = $(this).attr("site_id");
			
		    BootstrapDialog.show({
		          title: 'Clone Site',
		          message: '<label>Domain</label><br /><input class="form-control" id="clone_domain" name="clone_domain" value="">',
		          buttons: [{
		               label: '<a class="form-control">Clone Site</a>',
		              action: function(dialog) {
		                  // submit the form
						  clone_domain = $("#clone_domain").val();
						  site_to_clone_id = site_id;
						 
						  //activate_general_loader();
						  dialog.close();
						  activate_loader();
						  
						$.ajax({
							url: "/clone_site",
							dataType: 'JSON',
							type: 'POST',
							data: {clone_domain : clone_domain, site_to_clone_id: site_to_clone_id},
							success : function(result) {
								
								if(result["message"]){
	
									$.notify({
										icon: "",
										message: result["message"]

									},{
										type: 'info',
										timer: 4000
									});
									$("#loadMe").modal("hide");
									dialog.close();
								    return false;	
									
							  	}else{
									deactivate_loader();
									window.location.href = "/sites?success=true";
							  	}
								
							}		
						});		
						  
		              }
		          }]
		      });
			
		});
	   
	   
		$(".backup_action").click(function() {
			site_id = $(this).attr("site_id");
			
		    BootstrapDialog.show({
		          title: 'Backup Site',
		          message: '<label>Label</label><br /><input class="form-control" id="backup_label" name="backup_label" value="">',
		          buttons: [{
		               label: '<a class="form-control">Backup Site</a>',
		              action: function(dialog) {
		                  // submit the form
						  backup_label = $("#backup_label").val();
						 
						  //activate_general_loader();
						  dialog.close();
						  activate_loader();
						  
						$.ajax({
							url: "/wordpress_backups/create",
							dataType: 'JSON',
							type: 'POST',
							data: {backup_label : backup_label, site_id: site_id},
							success : function(result) {
								
								if(result["message"]){
	
									$.notify({
										icon: "",
										message: result["message"]

									},{
										type: 'info',
										timer: 4000
									});
									$("#loadMe").modal("hide");
									dialog.close();
								    return false;	
									
							  	}else{
									deactivate_loader();
									window.location.href = "/wordpress_backups";
							  	}
								
							}		
						});		
						  
		              }
		          }]
		      });
			
		});
		
		
		
	
    	$(document).ready(function(){
			
			load_banner();
			check_pete_version();
			
  			@if(isset($exporturl))
				console.log("download url: {{$exporturl}}");
  		  		window.location.assign("/{{$exporturl}}");
  		
  		  	@endif
				
			   @if(isset($success))
	
				@if($success == "true")
	
			    var delayInMilliseconds = 3000; //1 second

			    setTimeout(function() {
			      //your code to be executed after 1 second
			   	 $("#loadMe").modal("hide");
			    }, delayInMilliseconds);
	
			     $.ajax({
			           url: "/reload_server",
			           type: "get",
			           datatype: 'json',
			           success: function(data){
           
					       console.log("Reload server success!");

			           }

			     });

			    @endif
	 
			  @endif	
		  

    	});
				
</script>