@extends('layout')

@section('header')

	
@endsection

@section('content')
@include('error')

   
			
<form action="{{ route('sites.store') }}" id ="SiteForm" method="POST" enctype="multipart/form-data">

	<input type="hidden" name="_token" value="{{ csrf_token() }}">          
	<div class="row">
		<div class="col-md-12">
			
			<div class="page-header">
				<h3> Create New WordPress Instance</h3>
				
				
			
			</div>
			
			
	<p>Project name</p>
	<input type="text" id="name-field" name="name" class="form-control" value="" required="" aria-required="true"> <br />
                   								
									
			@if(isset($error))
			
			<div class="alert alert-danger">
				<p>There were some problems with your input.</p>
				<ul>
					@if($site->error_message1)
					<li><i class="glyphicon glyphicon-remove"></i>{{$site->error_message1}}</li>
					@endif
							
					@if($site->error_message2)
					<li><i class="glyphicon glyphicon-remove"></i>{{$site->error_message2}}</li>
					@endif
							
				</ul>
			</div>
			
			@endif
				
			
				 
		</div>
	</div>
							 
			
	@if($pete_options->get_meta_value('domain_template'))
							
						    
	<div class="row">
		<div class="col-md-12">
						
			
				<p>URL</p>
				<input type="text" id="url-field" name="url" class="inline_class url_wordpress_laravel" required/>
				<div id="url_wordpress_helper" class="inline_class">.{{$pete_options->get_meta_value('domain_template')}}</div>
				 <br /><br /><br />
			
		</div>
				
	</div>
							
	@else
						  
	<div class="row">
		<div class="col-md-12">
									
			<div class="form-group" id="url_div">
				<p>URL</p>
				<input type="text" id="url-field" name="url" class="form-control " value="{{ old("url") }}" required/>
				<br /><br /><br />
					   
			</div>
		</div>
	</div>
						  
	@endif
							
                    
               
	<button type="submit" id="create_button" class="btnpete">Create</button>
	<br /><br />
</form>
			
			
			

  
@endsection