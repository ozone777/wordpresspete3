@extends('layout')

@section('header')

@endsection

@section('content')

	@include('sites/_sites_header')
	
    <div class="row">
        <div class="col-md-12">
			
			
			<div class="content table-responsive">
            @if($sites->count())
                <table style="padding-left: 10px; padding-right: 10px;" class="table table-hover table-striped">
                    <thead>
                        <tr>
                         <th>Id</th>
                        <th>Project Name</th>
                        <th>Url</th>
                        <th>Action</th>
						<th>App</th>
                         <th class="text-right">Options</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($sites as $site)
                            <tr>
                                <td>{{$site->id}}</td>
                     <td>{{$site->name}}</td>           
                    <td>
					<a href="http://{{$site->url}}" target ='_blank'>{{$site->url}}</a>
					</td>
                   
                    <td>{{$site->action_name}}</td>
					<td>{{$site->app_name}}
					
						@if($site->app_name == "WordPress+Laravel")
							@if($site->action_name == "New")
							<br />
							Laravel version: {{$site->laravel_version}}
							@endif
						@endif
					
					</td>
					
                                <td class="text-right">
                                   
								    <a class="option_button" role="group" href="{{ route('sites.edit', $site->id) }}"> Options</a>
                                  
								   @if($site->app_name != "WordPress+Laravel")
								   <a class="option_button clone_action" id="clone_{{$site->name}}" site_id="{{$site->id}}" href="#"> Clone</a>
								   
								   <a class="option_button" href="/sites/export?id={{$site->id}}">Export</a>
								   
								   @if($pete_options->get_meta_value('obackups') == "active")
								   
								   <a class="option_button backup_action" id="create_backup_{{$site->name}}" site_id="{{$site->id}}" href="#">Create Backup</a>
								   
								   @endif
									 
									@if(!$site->suspend)
									<a class="option_button" id="suspend_{{$site->name}}" href="/sites/suspend?id={{$site->id}}">Suspend</a>
									@else
									<a class="option_button" id="continue_{{$site->name}}" href="/sites/site_continue?id={{$site->id}}">Continue</a>
									@endif
									
									@endif
									
                                    <form action="{{ route('sites.destroy', $site->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" id="delete_{{$site->name}}" class="option_button" style="background-color: #f1592a; width: 100%">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $sites->render() !!}
            @else
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                           	<th>Project Name</th>
                        	<th>Url</th>
                        	<th>Action</th>
							<th>App</th>
                         	<th class="text-right">Options</th>
                        </tr>
                    </thead>

                    <tbody>
					</tbody>
					
				</table>
            @endif
			</div>
        </div>
    </div>
	
	
	
	<div class="modal fade">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h4 class="modal-title">Modal title</h4>
	      </div>
	      <div class="modal-body">
			<form id="site_form" action="/snapshot_creation" style="display:none" method="POST">
				<input type="hidden" name="_method" value="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">         
			    <input type="hidden" id="snapshot_label_form" name="snapshot_label_form" value="">
				<input type="hidden" id="site_id_form" name="site_id_form" value="">
			    <input type="submit" />
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	@include('sites/_sites_js')

@endsection