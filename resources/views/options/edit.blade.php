@extends('layout')

@section('header')
   
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('options.update', $option->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @include('options/_form')
					
                 <button type="submit" id="update_button" class="btnpete">Update</button>
				 
            </form>

        </div>
    </div>
@endsection