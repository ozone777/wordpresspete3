<br />
     
<label for="option_name-field">Option_name</label>
<input type="text" id="option_name-field" name="option_name" class="form-control" value="{{ $option->option_name }}"/>
                      
<label for="option_value-field">Option_value</label>
<input type="text" id="option_value-field" name="option_value" class="form-control" value="{{ $option->option_value }}"/>

<label for="option_value-field">Order</label>
<input type="text" id="order" name="order" class="form-control" value="{{ $option->order }}"/>
                     
<label for="option_value-field">Category</label>
<input type="text" id="category" name="category" class="form-control" value="{{ $option->category }}"/>

<label for="option_value-field">Title</label>
<input type="text" id="title" name="title" class="form-control" value="{{ $option->title }}"/>

<label for="option_value-field">Version</label>
<input type="text" id="version" name="version" class="form-control" value="{{ $option->version }}"/>

<br />