@extends('layout')

@section('header')
  
@endsection

@section('content')

    <div class="row">

        <div class="col-md-12">
	        	
				<h1><a class="btn btn-success pull-right" href="/options/create"><i class="glyphicon glyphicon-plus"></i> Create option</a></h1>
	        
		</div>
	</div>

    <div class="row">
        <div class="col-md-12">
			
	        
			
			<div class="content table-responsive table-full-width">
			                               
            @if($options->count())
                 <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>OPTION_NAME</th>
                        <th>OPTION_VALUE</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($options as $option)
                            <tr>
                                <td>{{$option->id}}</td>
                                <td>{{$option->option_name}}</td>
                    <td>{{$option->option_value}}</td>
                                <td class="text-right">
                                    
                                    <a class="btn btn-xs btn-warning" id= "edit_{{$option->option_name}}" href="{{ route('options.edit', $option->id) }}">Edit</a>
                                    <form action="{{ route('options.destroy', $option->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" id="delete_{{$option->option_name}}" class="btn btn-xs btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $options->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif
			 </div>
        </div>
    </div>

@endsection