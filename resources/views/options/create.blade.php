@extends('layout')

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">
			
			<h3>Create new Option</h3>
			
            <form action="{{ route('options.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @include('options/_form')
               
                <button type="submit" id="create_button" class="btnpete">Create</button>
                
            </form>

        </div>
    </div>
@endsection