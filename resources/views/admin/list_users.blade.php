@extends('layout')

@section('content')
    <div class="row">
		
		
        <div class="col-md-12">
			
        <h1>
	       <a class="btn btn-success pull-right" href="/create_user"><i class="glyphicon glyphicon-plus"></i> Create user</a>
	    </h1>
			
            @if($users->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                        <th>Id</th>
                        <th>Name</th>
						
                        <th>Email</th>
                     
                    </thead>

                    <tbody>
                        @foreach($users as $user)
                            <tr>
                      <td>{{$user->id}}</td>
                     <td>{{$user->name}}</td>  
					 
                   	 <td>{{$user->email}}</td>           
                    
     
					
                                <td class="text-right">
                                   	
									<a class="btn btn-xs btn-warning" id="edit_{{$user->name}}" role="group" href="/edit_user?id={{$user->id}}">Edit</a>
									
                                    <form action="/delete_user" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                                    </form>
									
									
									
                                </td>
								
								
								
                            </tr>
                        @endforeach
                    </tbody>
                </table>
             
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>
	

@endsection