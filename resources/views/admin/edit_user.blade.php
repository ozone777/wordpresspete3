@extends('layout')

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">
			
			<h3>Edit user</h3>
			
		  	<form method="POST" action="/update_user">
		  	    {!! csrf_field() !!}
				<input type="hidden" name="user_id" value="{{$user->id}}">
				 @include('admin/_user_form')
				 
				 <button type="submit" id="update_button" class="btnpete">Update</button>
	
		  	</form>

        </div>
    </div>
@endsection
	
	
