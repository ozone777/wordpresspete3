@extends('layout')

@section('header')

@endsection

@section('content')

	
    <div class="row">
        <div class="col-md-12">
			<h3>PHPMYADMIN</h3>
			
			<p>User: root</p>
			<p>Password: {{$root_pass}}</p>
			
			<a target="_blank" href="{{$phpmyadmin_url}}"><img width = "250"src="/assets/img/phpmyadmin_logo.png"></a>
			
		</div>
	</div>

@endsection