@extends('layout')

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">
			
			<h3>Create user</h3>
			
			<p>Users created by the system administrator can only view and execute actions on the sites that have been created by them</p>
			
		  	<form method="POST" action="/store_user">
		  	    {!! csrf_field() !!}
				
				 @include('admin/_user_form')
				 
				 <button type="submit" id="create_button" class="btnpete">Create</button>
	
		  	</form>

        </div>
    </div>
@endsection
	
	
