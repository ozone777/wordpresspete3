@extends('layout')

@section('header')

@endsection

@section('content')

	
    <div class="row">
        <div class="col-md-12">
			<br />
			
			@if($os_distribution == "ubuntu")
			
				<p>PHP version must be >= 7.1 to create WordPress+Laravel integration.<p>
			
				<select class="form-control" name="php_version" id="php_version">
					
				  <option value="">Change PHP version</option>
				  <option value="php5.6">php5.6</option>
				  <option value="php7.0">php7.0</option>
				  <option value="php7.1">php7.1</option>
				  <option value="php7.2">php7.2</option>
				  <option value="php7.3">php7.3</option>
				  <option value="php7.4">php7.4</option>
				  
				</select>
				
			@elseif($os_distribution == "darwin")
			 
			@endif
			
			<br />
			
			<iframe height="10000" width="100%" src="/phpinfo_view"></iframe>
			
			
		</div>
	</div>
	
	<script>
		
		$( document ).ready(function() {
		 
		 
			$( "#php_version" ).change(function() {
			    php_version = $(this).val();
			 	$.ajax({
			 		url: "/change_php",
			 		dataType: 'JSON',
			 		type: 'POST',
					data: {_token: "{{ csrf_token() }}", php_version: php_version},
			 		success : function(result) {
						location.reload();
						window.location.href = "/phpinfo_panel?success=true";
		           }

		     	});
			  
			});
		 
		 	
		   @if(isset($success))
	
			@if($success == "true")
	
		    var delayInMilliseconds = 4000; //1 second
			activate_loader();
		    setTimeout(function() {
		      //your code to be executed after 1 second
		   	 $("#loadMe").modal("hide");
			 window.location.href = "/phpinfo_panel";
		    }, delayInMilliseconds);
	
		     $.ajax({
		           url: "/reload_server",
		           type: "get",
		           datatype: 'json',
		           success: function(data){
           
				       console.log("Reload server success!");

		           }

		     });

		    @endif
	 
		  @endif	
		 
		});
		
	 	
	</script>

@endsection