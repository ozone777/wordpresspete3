@extends('layout')

@section('header')

<style>
	
	.form-inline{
		  display: inline-block;
	}
	
	h5{
		font-size: 15px;
	}
	
	p{
		font-size: 11px;
	}
	
	.new_version_notice{
		padding: 7px 7px 7px 7px;
		background-attachment: scroll;
		background-clip: border-box;
		background-color: rgb(255, 248, 229);
		background-image: none;
		background-origin: padding-box;
		background-position: 0% 0%;
		background-position-x: 0%;
		background-position-y: 0%;
		background-repeat: repeat;
		background-size: auto;
		border-bottom-color: rgb(204, 208, 212);
		border-bottom-style: solid;
		border-bottom-width: 1px;
		border-image-outset: 0;
		border-image-repeat: stretch;
		border-image-slice: 50%;
		border-image-source: none;
		border-image-width: 1;
		border-left-color: rgb(255, 185, 0);
		border-left-style: solid;
		border-left-width: 4px;
		border-right-color: rgb(204, 208, 212);
		border-right-style: solid;
		border-right-width: 1px;
		border-spacing: 0px 0px;
		border-top-color: rgb(204, 208, 212);
		border-top-style: solid;
		border-top-width: 1px;
		box-shadow: none;
		color: rgb(0, 0, 0);
		font-size: 13px;
		font-weight: 400;
		line-height: 19.5px;
		margin-bottom: 7px;
		margin-left: 7px;
		margin-right: 7px;
		margin-top: 5px;
		outline-color: rgb(0, 0, 0);
		outline-style: none;
		outline-width: 0px;
		overflow-wrap: break-word;
		padding-bottom: 1px;
		padding-left: 7px;
		padding-right: 7px;
		padding-top: 1px;
		margin-bottom: 7px
	}
	
	.button {
	    color: #0071a1;
	    border-color: #0071a1;
	    background: #f3f5f6;
	    vertical-align: top;
		display: inline-block;
		text-decoration: none;
		font-size: 13px;
		line-height: 2.15384615;
		min-height: 30px;
		margin: 0;
		padding: 0 10px;
		cursor: pointer;
		border-width: 1px;
		border-style: solid;
		-webkit-appearance: none;
		border-radius: 3px;
		white-space: nowrap;
		box-sizing: border-box;
		margin-left: 10px;
		margin-bottom: 15px;
		
	}
	
	code{
		font-size: 11px;
	}
	
</style>

@endsection

@section('content')
@include('error')

<div class="pete_plugins">

	  <div class="row">
		  
		  <div class="col-md-12">

	 		 <h3 class="plugins_page_title">WordPress Pete Premium Plugins</h3>
			 
		 </div>
	</div>
	
	<?php $cont =0;
	$base_path = base_path();
	?>
	
	<div class="container-fluid">
		<div class="row">
	
	@foreach($pete_plugins as $plugin)
	
	
	<?php
	
		$plugin_version = $pete_options->get_meta_version($plugin->option_name);
		
		if($os_distribution == "darwin"){
			
			$uninstall_script = $plugin->mac_uninstall_script;
			$install_script = $plugin->mac_install_script;
			$update_script = $plugin->mac_update_script;
			
		}else if($os_distribution == "ubuntu"){
			
			$uninstall_script = $plugin->olinux_uninstall_script;
			$install_script = $plugin->olinux_install_script;
			$update_script = $plugin->olinux_update_script;
			
		}else if($os_distribution == "win"){
			
			$uninstall_script = $plugin->win_uninstall_script;
			$install_script = $plugin->win_install_script;
			$update_script = $plugin->win_update_script;
			
		}
		
		if($pete_environment == "development"){
			$install_script = $plugin->development_script;
		}
			
	?>

	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="card">
			<img src="{{$dashboard_url}}/plugins_images/{{$plugin->image}}" width="100%">
			<div class="card-body">
				<h5 style="padding-left: 10px;" class="card-title">{{$plugin->title}}</h5>
				<?php
					$current_plugin_version = $pete_options->get_meta_version($plugin->option_name);
				?>
				@if($current_plugin_version != "")
				<h5 style="padding-left: 10px;" class="card-title">Version: {{$current_plugin_version}}</h5>
				@else
				<h5 style="padding-left: 10px;" class="card-title">Version: {{$plugin->version}}</h5>
				@endif
				<div class="plugin_description">
				<p class="card-text">
					{{$plugin->description}}
				</p>
				</div>
				
				
				@if(!is_dir($base_path."/vendor/".$plugin->package_folder))
				
					<?php $div_id = str_replace(" ","",$plugin->title);?>
				
					<form class="form-inline" action="/pete_plugins_install" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="script" value="{{$install_script}}">
						<input type="hidden" name="redirect_after_install" value="{{$plugin->redirect_after_install}}">
						<button type="submit" id="{{$div_id}}" class="button">Install Now</button>
					</form>
				
				@else
				
					<form class="form-inline" action="/pete_plugins_install" method="POST">
						<input type="hidden" name="script" value="{{$uninstall_script}}">
						<button type="submit" class="button">Uninstall</button>
					</form>
					
					<?php
					
					//$pete_options->get_meta_value('wordpress_plus_laravel');
					
					?>
					@if($pete_options->get_meta_version($plugin->option_name) != $plugin->version )
					   
					   	
					   
						<form class="form-inline" action="/pete_plugins_update" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="script" value="{{$update_script}}">
							<button type="submit" class="button">Update</button> 
							
						</form>
						<p class="new_version_notice">There is a new version of {{$plugin->title}}: {{$plugin->version}}</p>
						
						
					@endif
				
				@endif
	
				
	
			</div>
		</div>
	</div>


	@endforeach
	
</div>
</div>
	  
	  <div class="row">
		  
		  <div class="col-md-6">

	 		 <h3 class="plugins_page_title">WordPress Plugins</h3>
			 
		 </div>
	</div>
	
	<div class="container-fluid">
		<div class="row">
	
	@foreach($wp_plugins as $plugin)
	
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="card">
				<img src="{{$dashboard_url}}/wp_plugins_images/{{$plugin->image}}" width="100%">
				<div class="card-body">
					<h5 style="padding-left: 10px;" class="card-title">{{$plugin->title}}</h5>
					<div class="plugin_description">
						<p class="card-text">
						{{$plugin->description}}
					</p>
					</div>
				
					<a href="{{$plugin->url}}" class="button">Download</a>
				
				</div>
			</div>
		</div>	
	
	@endforeach
	  
</div>	  
    
	

@endsection