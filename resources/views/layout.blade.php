<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	
	<title>WordPress Pete</title>
	
	<link rel="icon" type="image/png" href="/favicon/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
	<link rel="manifest" href="/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>

    <!--  Custom CSS    -->
    <link href="/assets/css/custom.css" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" href="/assets/css/jquery.simplebanner.css"/>
    <!--     Fonts and icons     -->
	
    <!--   Core JS Files   -->
	<script src="/assets/js/jquery-1.10.2.min.js" type="text/javascript"></script>
	<script src="/assets/js/jquery.simplebanner.min.js" type="text/javascript"></script>
	
	
	<script src="/js/jquery.validate.min.js"></script>
	
	
	
	 @yield('header')

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="blue" data-image="/assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo"><img style="display: inline; width: 190px" src="/pete_logo_header.png">
				
				 v&nbsp;{{$pete_options->get_meta_value('version')}}</div>
			
	

            <ul class="nav">
				
				 @foreach($sidebar_options as $option)
				 
					@if($viewsw == $option->option_value)
					   <li class="active">
					@else
					   <li>
					@endif
	                    <a href="{{$option->option_value}}">
	                        <i class="pe-7s-browser"></i>
	                        <p>{{$option->title}}</p>
	                    </a>
	                </li>
				 
				 @endforeach
				 
				 @if($current_user->admin)
				   @if($viewsw == "/list_users")
					 <li class="active">	
					@else
					<li>
					@endif
                    <a href="/list_users">
                        <i class="pe-7s-browser"></i>
                        <p>Users</p>
                    </a>
					</li>
					
 				   @if($viewsw == "/options")
 					 <li class="active">	
 					@else
 					<li>
 					@endif
                     <a href="/options">
                         <i class="pe-7s-browser"></i>
                         <p>Options</p>
                     </a>
 					</li>
					
					
				 @endif
				
               
            </ul>
			
			<div id="loading_area">
				
				
				
			</div>
			
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
 						
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
						
                        <li id="update_pete">
                            
                        </li>
						
                        <li>
							<a  href="/register_your_license">
							    Register
							</a>
                           
                        </li>
						
						<li>
							
                             <a href="/logout">
                                 Log out
                             </a>
							
						</li>
						
						
                        
						
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">

		       
		        @yield('content')

            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
					
					
                     
				   <p style="padding-top: 5px; font-size:12px;">
					   
					   @if(isset($system_vars["apache_v_dialog"]))
                         {{$system_vars["apache_v_dialog"]}}
					   @endif
					    
					   <br />
					   @if(isset($system_vars["mysql_v_dialog"]))
                         {{$system_vars["mysql_v_dialog"]}}
					   @endif
					 
                     </p>
				  
				
                    
                </nav>
                <p class="copyright pull-right">
					 <a href="http://wordpresspete.com">wordpresspete.com</a>
                    
                </p>
            </div>
        </footer>

    </div>
</div>



</body>

 
	<script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>
	
	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="/assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="/assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    -->
		
    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="/assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="/assets/js/demo.js"></script>
	
	<script src="/assets/js/bootstrap-dialog.js"></script>
	
	<script type="text/javascript">
		
	
	///////
	function activate_loader()
	{
			//Add Loader
		    $("#loadMe").modal({
		      backdrop: "static", //remove ability to close modal with click
		      keyboard: false, //remove option to close with keyboard
		      show: true //Display loader!
		    });
	}	
	
	function deactivate_loader()
	{
			//Add Loader
		   $("#loadMe").modal("hide");
	}	
	
	
		
	
	$( document ).ajaxStart(function() {
		
		//activate_loader();
		
	});
	
	$(document).ajaxSuccess(function() {
	
	 // $("#loadMe").modal("hide");
	  
	});
	
	$("#create_button").click(function() {
		activate_loader();
	});
	
	
	
	$(document).ready(function(){
	
 
	 
	 });

	</script>
	
	
	
	<!-- Modal -->
	<div class="modal fade" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="loadMeLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	      <div class="modal-body text-center">
	        <div class="loader"></div>
	       
	      </div>
	    </div>
	  </div>
	</div>
	
	
	
</html>
