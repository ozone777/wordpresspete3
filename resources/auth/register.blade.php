<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
	
	<link rel="icon" type="image/png" href="/favicon/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
	<link rel="manifest" href="/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
    <title>Welcome to WordPress Pete!</title>

    <link rel="stylesheet" href="/css/style.css">
	
	<style>
	.alert-danger{
		background-color: #f05a27;
		color: #fff;
	}	
	</style>

  </head>

  <body>

    <!--Google Font - Work Sans-->
<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,700' rel='stylesheet' type='text/css'>

<div class="container">
	
	@include('error')
	
  <div class="profile">
    <button class="profile__avatar" id="toggleProfile">
     <img src="/face.png" alt="WordpressPete" style="width:58px;height:70px;">
	<p style="padding-left: 25px;">Create Admin</p>
    </button>
	
	<form method="POST" action="/auth/register">
	    {!! csrf_field() !!}
	
    <div class="profile__form">
      <div class="profile__fields">
        <div class="field">
			<input type="text" name="name" class="input" value="{{ old('name') }}">
			<label for="fieldUser">Username</label>
        </div>
        <div class="field">
           <input type="email" name="email" class="input" value="{{ old('email') }}">
		   <label for="fieldUser">Email</label>
        </div>
		
        <div class="field">
          <input type="password" class="input" name="password">
		  <label for="fieldUser">Password</label>
        </div>
		
        <div class="field">
          <input type="password" class="input" name="password_confirmation">
		  <label for="fieldUser" >Confirm Password</label>
        </div>
		
		@if($pete_demo == "active")
		
        	<div class="field">
        	  <input type="checkbox" id="contact_me_again" name="contact_me_again" value="1">
			  <label for="fieldUser" >Please let me know about Pete’s latest updates via email</label>
        	</div>
		@endif
		
        <div class="profile__footer">
          <button type="submit" id="register_action" class="btn">Register</button>
        </div>
      </div>
     </div>
  </div>

	</form>

</div>
    
        <script src="/js/index.js"></script>

   
  </body>
</html>
